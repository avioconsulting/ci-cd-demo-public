@Application
Feature: Sample feature file that interacts with an Appian application

  Background: Login to Appian environment
    Given I setup with "CHROME" browser
#   TODO: Fill in with your Appian URL with /suite/sites/acd-main-page included in the URL
    When I set appian URL to "https://<<yourSiteName>>.appiancloud.com/suite/sites/acd-main-page"
    And I set appian version to "20.1"
    And I set appian locale to "en_US"
#   TODO: configure src/main/resources/configs role.admin to an admin user for the application
    Then I login with role "role.basicUser"

  Scenario: Clicking on cards does not break page
    Given I click on card "Cereals"
    Given I click on card "Beverages"
    Given I click on card "Culinary"
    Given I click on card "Infant Nutrition"
